import {
  isArray,
  isString,
  ValidateBy,
  ValidationArguments,
} from 'class-validator';

export const IsArrayOrString = (): PropertyDecorator => {
  return ValidateBy({
    name: 'isArrayOrString',
    validator: {
      validate(value: unknown): boolean {
        return isArray(value) || isString(value);
      },
      defaultMessage(validationArguments?: ValidationArguments): string {
        return `${validationArguments.property} must be array or string`;
      },
    },
  });
};
