import { ValidateBy, ValidationArguments } from 'class-validator';

export const IsNullString = (): PropertyDecorator => {
  return ValidateBy({
    name: 'isNullString',
    validator: {
      validate(value: string): boolean {
        return value === 'null';
      },
      defaultMessage(validationArguments?: ValidationArguments): string {
        return `${validationArguments.property} is not null string`;
      },
    },
  });
};
