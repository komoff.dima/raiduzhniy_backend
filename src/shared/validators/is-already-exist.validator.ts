import { Inject, Injectable, Type } from '@nestjs/common';
import { Provider } from '@nestjs/common/interfaces/modules/provider.interface';
import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { MongoDbBase } from '../abstract-classes';
import { IS_ALREADY_EXIST_VALIDATION_SERVICE } from '../tokens';

interface IsAlreadyExistValidationOptions extends ValidationOptions {
  regexpOptions?: string;
}

@ValidatorConstraint({ name: 'isAlreadyExist', async: true })
@Injectable()
export class IsAlreadyExistConstraint<DbService extends MongoDbBase<unknown>>
  implements ValidatorConstraintInterface
{
  constructor(
    @Inject(IS_ALREADY_EXIST_VALIDATION_SERVICE) private dbService: DbService,
  ) {}

  validate(
    value: string,
    { property, constraints }: ValidationArguments,
  ): Promise<boolean> | boolean {
    const [regexpOptions] = constraints || [];
    return this.dbService.checkAlreadyExist({
      [property]: {
        $regex: `^${value}$`,
        ...(regexpOptions ? { $options: regexpOptions } : {}),
      },
    });
  }

  defaultMessage(validationArguments?: ValidationArguments): string {
    return `${validationArguments.property} already exist`;
  }
}

export function IsAlreadyExist({
  regexpOptions,
  ...options
}: IsAlreadyExistValidationOptions = {}) {
  return function (object: unknown, propertyName: string) {
    registerDecorator({
      target: object.constructor,
      propertyName: propertyName,
      options,
      constraints: [regexpOptions],
      validator: IsAlreadyExistConstraint,
    });
  };
}

export const createIsAlreadyExistValidatorProviders = <
  Service extends MongoDbBase<unknown>,
>(
  dbService: Type<Service>,
): Provider[] => [
  IsAlreadyExistConstraint,
  {
    provide: IS_ALREADY_EXIST_VALIDATION_SERVICE,
    useClass: dbService,
  },
];
