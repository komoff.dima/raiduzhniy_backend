import { Schema } from 'mongoose';

export class SchemaUtils {
  static setup<T>(schema: Schema<T>): void {
    schema.set('toJSON', {
      virtuals: true,
      versionKey: false,
      transform: (doc, rec) => {
        rec.id = rec._id.toString();
        delete rec._id;
      },
    });
  }
}
