export class ArrayUtils {
  static partition<T>(arr: T[], isValid: (item: T) => boolean) {
    return arr.reduce(
      ([pass, fail], elem) => {
        return isValid(elem)
          ? [[...pass, elem], fail]
          : [pass, [...fail, elem]];
      },
      [[], []],
    );
  }
}
