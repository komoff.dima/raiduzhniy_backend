import { IFile } from '../interfaces';

const IMAGES_MIME_TYPES = [
  'image/bmp',
  'image/jpeg',
  'image/x-png',
  'image/png',
  'image/gif',
  'image/webp',
];

export class FilesUtil {
  static isImageFile(file: IFile): boolean {
    return IMAGES_MIME_TYPES.includes(file.mimetype);
  }
}
