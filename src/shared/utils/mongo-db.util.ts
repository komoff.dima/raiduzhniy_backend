import { FilterQuery, QueryWithHelpers } from 'mongoose';
import { DEFAULT_GET_MANY_OPTIONS, FILTER_QUERIES_MAPPER } from '../constants';
import { OrderDirection } from '../enums';
import {
  BuildQueryParams,
  Dictionary,
  PaginateParams,
  PaginateQueryParams,
} from '../interfaces';
import { IFilterValue } from '../interfaces/filters.interface';

export class MongoDbUtil {
  static adaptFilterQuery<T>(
    filters: Dictionary<IFilterValue>,
  ): FilterQuery<T> {
    return Object.entries(filters).reduce(
      (adaptedQuery, [key, { value, type }]) => {
        const adapter = FILTER_QUERIES_MAPPER[type];

        return {
          ...adaptedQuery,
          [key]: adapter ? adapter(value) : value,
        };
      },
      {},
    );
  }

  static getPaginateQueryParams({
    orderBy,
    orderDirection,
    pageSize,
    pageNumber,
  }: PaginateQueryParams): PaginateParams {
    return {
      ...(orderBy
        ? { sort: { [orderBy]: orderDirection || OrderDirection.Acs } }
        : {}),
      limit: pageSize
        ? +pageSize
        : +DEFAULT_GET_MANY_OPTIONS.queryParams.pageSize,
      skip: pageNumber ? (+pageNumber - 1) * +pageSize : 0,
    };
  }

  static buildQuery<Type>(
    entryQuery: QueryWithHelpers<unknown, unknown>,
    queryParams: BuildQueryParams,
  ): QueryWithHelpers<Type, unknown> {
    return Object.entries(queryParams).reduce(
      (query, [queryKey, queryValue]) => query[queryKey](queryValue),
      entryQuery,
    );
  }
}
