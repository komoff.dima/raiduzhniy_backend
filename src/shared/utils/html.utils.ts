import { Provider } from '@nestjs/common/interfaces/modules/provider.interface';
import { HtmlDocumentAttributes } from '../interfaces';
import { HtmlsService } from '../modules/htmls';
import { HTML_DOCUMENT_ATTRIBUTES } from '../tokens';

const attributesFactory = async (
  htmlService: HtmlsService,
  docName?: string,
): Promise<HtmlDocumentAttributes> => {
  if (!docName) {
    throw new Error('docName param did not set');
  }
  return {
    id: await htmlService.getDocId(docName),
    uniqueName: docName,
  };
};

export class HtmlUtils {
  static getHtmlDocAttributesProviders(docName: string): Provider[] {
    return [
      {
        provide: 'DOCUMENT_NAME',
        useValue: docName,
      },
      {
        provide: HTML_DOCUMENT_ATTRIBUTES,
        useFactory: attributesFactory,
        inject: [HtmlsService, { token: 'DOCUMENT_NAME', optional: true }],
      },
    ];
  }
}
