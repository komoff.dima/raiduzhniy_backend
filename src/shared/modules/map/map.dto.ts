import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class MapMarkerDto {
  @IsNotEmpty()
  @IsNumber()
  lat: number;

  @IsNotEmpty()
  @IsNumber()
  long: number;

  @IsNotEmpty()
  @IsString()
  caption: string;
}
