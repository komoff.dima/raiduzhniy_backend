import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { SseService } from '../../../controllers/sse/sse.service';
import { DefaultResponse, ReceivedDocuments } from '../../interfaces';
import { MapMarker } from './map.schema';
import { MongoDbBase } from '../../abstract-classes/mongo-db.base';
import { MapMarkerDto } from './map.dto';

@Injectable()
export class MapService extends MongoDbBase<MapMarker> {
  constructor(
    @InjectModel(MapMarker.name) mapMarkerModel: Model<MapMarker>,
    sseService: SseService,
  ) {
    super(mapMarkerModel, sseService);
  }

  getMarkers(): Promise<ReceivedDocuments<MapMarker>> {
    return this.getMany();
  }

  async addMarker(markerDto: MapMarkerDto): Promise<MapMarker> {
    const existingMarker = await this.getOne({
      filter: {
        $and: [{ lat: markerDto.lat }, { long: markerDto.long }],
      },
    });

    if (existingMarker) {
      throw new BadRequestException('Точка з такими координатами вже існує');
    }

    return this.createNewRecord(markerDto);
  }

  deleteMarker(markerId: string): Promise<DefaultResponse> {
    return this.findByIdAndDelete(markerId);
  }
}
