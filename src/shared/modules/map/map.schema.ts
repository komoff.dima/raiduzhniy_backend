import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { SchemaBase } from '../../abstract-classes';
import { SchemaUtils } from '../../utils';
import { HydratedDocument } from 'mongoose';

export type MapSchema = HydratedDocument<MapMarker>;

@Schema()
export class MapMarker extends SchemaBase {
  @Prop({ type: Number, required: true })
  lat: number;

  @Prop({ type: Number, required: true })
  long: number;

  @Prop({ type: String, required: true })
  caption: string;
}

export const MapMarkerSchema = SchemaFactory.createForClass(MapMarker);

SchemaUtils.setup(MapMarkerSchema);
