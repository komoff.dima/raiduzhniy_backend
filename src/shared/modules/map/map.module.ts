import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MapMarker, MapMarkerSchema } from './map.schema';
import { MapService } from './map.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: MapMarker.name, schema: MapMarkerSchema },
    ]),
  ],
  providers: [MapService],
  exports: [MapService],
})
export class MapModule {}
