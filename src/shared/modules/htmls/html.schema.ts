import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';
import { SchemaBase } from '../../abstract-classes';
import { SchemaUtils } from '../../utils';

export type OwnerSchema = HydratedDocument<Html>;

export class BaseHtml extends SchemaBase {
  @Prop({ type: String, required: true })
  title: string;

  @Prop({ type: String, required: true })
  html: string;
}

@Schema()
export class Html extends BaseHtml {
  @Prop({ type: String, unique: true })
  uniqueName: string;
}

export const HtmlSchema = SchemaFactory.createForClass(Html);

SchemaUtils.setup(HtmlSchema);
