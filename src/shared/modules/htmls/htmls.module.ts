import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Html, HtmlSchema } from './html.schema';
import { HtmlsService } from './htmls.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Html.name, schema: HtmlSchema }]),
  ],
  providers: [HtmlsService],
  exports: [HtmlsService],
})
export class HtmlsModule {}
