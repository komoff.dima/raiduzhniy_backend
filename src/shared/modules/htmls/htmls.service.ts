import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { SseService } from '../../../controllers/sse/sse.service';
import { MongoDbBase } from '../../abstract-classes/mongo-db.base';
import { HtmlDto } from './html.dto';
import { Html } from './html.schema';

@Injectable()
export class HtmlsService extends MongoDbBase<Html> {
  constructor(
    @InjectModel(Html.name) model: Model<Html>,
    sseService: SseService,
  ) {
    super(model, sseService);
  }

  editDoc(docId: string, htmlDto: HtmlDto): Promise<Html> {
    return this.findByIdAndUpdate(docId, htmlDto);
  }

  getDoc(docId: string): Promise<Html> {
    return this.getById(docId);
  }

  createDoc(htmlDto: HtmlDto, uniqueName: string): Promise<Html> {
    return this.createNewRecord({
      ...htmlDto,
      uniqueName,
    });
  }

  async getDocId(uniqueName: string): Promise<string> {
    const html = await this.getOne({ filter: { uniqueName } });

    return html?.id;
  }
}
