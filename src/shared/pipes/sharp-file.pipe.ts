import {
  ArgumentMetadata,
  Inject,
  Injectable,
  Optional,
  PipeTransform,
} from '@nestjs/common';
import { IFile, SharpOptions } from '../interfaces';
import * as sharp from 'sharp';
import { SHARP_OPTIONS } from '../tokens/files.token';
import { FilesUtil } from '../utils';

abstract class SharpPipeBase {
  protected constructor(private options: SharpOptions = {}) {}

  protected async optimizeImage(image: IFile): Promise<IFile> {
    return {
      ...image,
      buffer:
        image && FilesUtil.isImageFile(image)
          ? await this.sharpImage(image.buffer)
          : image.buffer,
    };
  }
  private sharpImage(imageBuffer: Buffer): Promise<Buffer> {
    const { size, quality } = this.options || {};

    let sharped = sharp(imageBuffer);

    if (size) {
      sharped = sharped.resize(size.width, size.height);
    }

    return sharped.jpeg({ force: true, quality, mozjpeg: true }).toBuffer();
  }
}

@Injectable()
export class SharpFilePipe
  extends SharpPipeBase
  implements PipeTransform<IFile, Promise<IFile>>
{
  constructor(@Optional() @Inject(SHARP_OPTIONS) options: SharpOptions) {
    super(options);
  }
  transform(file: IFile, metadata: ArgumentMetadata): Promise<IFile> {
    return this.optimizeImage(file);
  }
}

@Injectable()
export class SharpFilesPipe
  extends SharpPipeBase
  implements PipeTransform<IFile[], Promise<IFile[]>>
{
  constructor(@Optional() @Inject(SHARP_OPTIONS) options: SharpOptions) {
    super(options);
  }

  transform(files: IFile[], metadata: ArgumentMetadata): Promise<IFile[]> {
    return Promise.all(
      files.map(async (file) => await this.optimizeImage(file)),
    );
  }
}
