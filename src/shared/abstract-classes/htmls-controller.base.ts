import { Body, Get, Post, Put } from '@nestjs/common';
import { UserRole } from '../../controllers/users/users.enum';
import { IsPublicRequest, Roles } from '../decorators';
import { HtmlDocumentAttributes } from '../interfaces';
import { Html, HtmlDto, HtmlsService } from '../modules/htmls';

export abstract class HtmlsControllerBase {
  private documentId: string = this.attributes.id;
  private readonly documentUniqueName: string = this.attributes.uniqueName;

  protected constructor(
    private htmlsService: HtmlsService,
    private attributes: HtmlDocumentAttributes,
  ) {}

  @Post('create')
  @Roles(UserRole.Admin, UserRole.Superadmin)
  async createDoc(@Body() htmlDto: HtmlDto): Promise<Html> {
    const newDoc = await this.htmlsService.createDoc(
      htmlDto,
      this.documentUniqueName,
    );
    this.documentId = newDoc.id;

    return newDoc;
  }

  @Put('edit')
  @Roles(UserRole.Admin)
  async editDoc(@Body() htmlDto: HtmlDto): Promise<Html> {
    return this.htmlsService.editDoc(this.documentId, htmlDto);
  }

  @Get()
  @IsPublicRequest()
  async getDoc(): Promise<Html> {
    return this.htmlsService.getDoc(this.documentId);
  }
}
