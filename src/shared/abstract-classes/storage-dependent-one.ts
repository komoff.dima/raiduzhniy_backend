import { Document, Model } from 'mongoose';
import { SseService } from '../../controllers/sse/sse.service';
import {
  DBParams,
  DefaultResponse,
  GetManyOptions,
  IFile,
  IStorageDependentOne,
  ReceivedDocuments,
  StorageDependent,
} from '../interfaces';
import { StorageType } from '../modules/firebase/storage';
import { FilesService } from '../modules/images/files.service';
import { MongoDbBase } from './mongo-db.base';
import { StorageDependentSchemaBase } from './schema.base';

export abstract class StorageDependentOne<T extends StorageDependentSchemaBase>
  extends MongoDbBase<T>
  implements StorageDependent<T, IStorageDependentOne<T>>
{
  protected abstract readonly storageType: StorageType;
  protected constructor(
    documentTypeModel: Model<T>,
    sseService: SseService,
    private filesService: FilesService,
  ) {
    super(documentTypeModel, sseService);
  }

  async createRecord(dto, file: IFile): Promise<T> {
    const storagePath = file
      ? await this.filesService.uploadFile(file, this.storageType)
      : null;

    return this.createNewRecord({
      ...dto,
      storagePath,
    } as unknown as T);
  }

  async deleteRecord(id: string): Promise<DefaultResponse> {
    const record = await this.getById(id);

    try {
      await Promise.all([
        ...(record?.storagePath
          ? [this.filesService.deleteFile(record.storagePath)]
          : []),
        this.findByIdAndDelete(id),
      ]);

      return { success: true };
    } catch (error) {
      return { success: false };
    }
  }

  async getRecords(
    options?: GetManyOptions<T>,
  ): Promise<ReceivedDocuments<IStorageDependentOne<T>>> {
    const receivedDocuments = await this.getMany(options);

    const promises = receivedDocuments.elements.map((doc) => {
      return this.transformStoragePathToUrl(doc as unknown as Document);
    });

    return {
      ...receivedDocuments,
      elements: await Promise.all(promises),
    };
  }

  async getRecord(
    id: string,
    dbParams?: DBParams,
  ): Promise<IStorageDependentOne<T>> {
    const record = await this.getById(id, dbParams);

    return this.transformStoragePathToUrl(record as unknown as Document);
  }

  async editRecord<EditDto extends { file?: 'null' }>(
    id: string,
    editDto: EditDto,
    newFile?: IFile,
  ): Promise<T> {
    const { file, ...restDto } = editDto;

    let storagePath: string;
    const existedRecord = await this.getById(id);

    if (newFile || file === 'null') {
      existedRecord.storagePath &&
        (await this.filesService
          .deleteFile(existedRecord?.storagePath)
          .catch((error) => {
            if (error.code !== 'storage/object-not-found') {
              throw error;
            }
          }));

      storagePath = null;
    }

    if (newFile) {
      storagePath = await this.filesService.uploadFile(
        newFile,
        this.storageType,
      );
    }

    return this.findByIdAndUpdate(id, {
      ...(restDto as unknown as T),
      ...(storagePath || storagePath === null ? { storagePath } : {}),
    });
  }

  private async transformStoragePathToUrl(
    doc: Document,
  ): Promise<IStorageDependentOne<T>> {
    const record: T = doc['_doc'];
    const storagePath = record.storagePath;
    record.id = record['_id'];
    delete record['_id'];
    delete record['__v'];
    delete record['storagePath'];

    return {
      ...record,
      file: {
        url: await this.filesService
          .getDownloadLink(storagePath)
          .catch(() => null),
        storagePath,
      },
    };
  }
}
