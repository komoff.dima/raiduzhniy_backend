import { Prop } from '@nestjs/mongoose';

export abstract class SchemaBase {
  @Prop({ type: String, default: null, select: false })
  createdAt?: string;

  @Prop({ type: String, default: null, select: false })
  editedAt?: string;

  id?: string;
}

export abstract class StorageDependentSchemaBase extends SchemaBase {
  @Prop({ type: String, default: '' })
  storagePath: string;
}

export abstract class StorageDependentManySchemaBase extends SchemaBase {
  @Prop([{ type: String, default: '' }])
  storagePaths: string[];
}
