import { BadRequestException } from '@nestjs/common';
import { Document, Model } from 'mongoose';
import { SseService } from '../../controllers/sse/sse.service';
import {
  DBParams,
  DefaultResponse,
  GetManyOptions,
  IFile,
  IStorageDependentMany,
  ReceivedDocuments,
  StorageDependent,
} from '../interfaces';
import { StorageType } from '../modules/firebase/storage';
import { FilesService } from '../modules/images/files.service';
import { ArrayUtils } from '../utils';
import { MongoDbBase } from './mongo-db.base';
import { StorageDependentManySchemaBase } from './schema.base';

export abstract class StorageDependentMany<
    T extends StorageDependentManySchemaBase,
  >
  extends MongoDbBase<T>
  implements StorageDependent<T, IStorageDependentMany<T>>
{
  protected abstract readonly storageType: StorageType;
  protected constructor(
    documentTypeModel: Model<T>,
    sseService: SseService,
    private filesService: FilesService,
  ) {
    super(documentTypeModel, sseService);
  }

  async createRecord(dto, files: IFile[]): Promise<T> {
    const storagePathPromises: Promise<string>[] = files.map((file) =>
      this.filesService.uploadFile(file, this.storageType),
    );

    return this.createNewRecord({
      ...dto,
      storagePaths: await Promise.all(storagePathPromises),
    });
  }

  async deleteRecord(id: string): Promise<DefaultResponse> {
    const record = await this.getById(id);

    try {
      await Promise.all([
        ...record.storagePaths.map((path) =>
          this.filesService.deleteFile(path),
        ),
        this.findByIdAndDelete(id),
      ]);

      return { success: true };
    } catch (error) {
      return { success: false };
    }
  }

  async getRecords(
    options?: GetManyOptions<T>,
  ): Promise<ReceivedDocuments<IStorageDependentMany<T>>> {
    const receivedDocuments = await this.getMany(options);

    const promises = receivedDocuments.elements.map((doc) => {
      return this.transformStoragePathToUrl(doc as unknown as Document);
    });

    return {
      ...receivedDocuments,
      elements: await Promise.all(promises),
    };
  }

  async getRecord(
    id: string,
    dbParams?: DBParams,
  ): Promise<IStorageDependentMany<T>> {
    const record = await this.getById(id, dbParams);

    return this.transformStoragePathToUrl(record as unknown as Document);
  }

  async editRecord<EditDto extends { filesToDelete?: string[] | string }>(
    id: string,
    editDto: EditDto,
    newFiles?: IFile[],
  ): Promise<T> {
    const { filesToDelete, ...restDto } = editDto;

    let newStoragePaths: string[];
    const existedRecord = await this.getById(id);

    const [storagePathsToDelete, storagePathsToLeave] = ArrayUtils.partition(
      existedRecord.storagePaths,
      (path) => (filesToDelete || []).includes(path),
    );

    if (storagePathsToDelete.length) {
      await Promise.all(
        storagePathsToDelete.map((path) =>
          this.filesService.deleteFile(path).catch((error) => {
            if (error.code !== 'storage/object-not-found') {
              throw error;
            }
          }),
        ),
      );
    }

    if (newFiles) {
      newStoragePaths = await Promise.all(
        newFiles.map((file) =>
          this.filesService.uploadFile(file, this.storageType),
        ),
      );
    }

    return this.findByIdAndUpdate(id, {
      ...(restDto as unknown as T),
      storagePaths: [...storagePathsToLeave, ...newStoragePaths],
    });
  }

  async deleteFileFromRecord(id: string, fileStoragePath: string): Promise<T> {
    const record = await this.getById(id);

    const existPath = record.storagePaths.includes(fileStoragePath);

    if (!existPath) {
      throw new BadRequestException('Incorrect storagePath');
    }

    await this.filesService.deleteFile(fileStoragePath);

    record.storagePaths = record.storagePaths.filter(
      (path) => path !== fileStoragePath,
    );

    return this.findByIdAndUpdate(id, record);
  }

  async addFileToRecord(id: string, file: IFile): Promise<T> {
    const record = await this.getById(id);

    const storagePath = await this.filesService.uploadFile(
      file,
      this.storageType,
    );

    record.storagePaths = [...record.storagePaths, storagePath];

    return this.findByIdAndUpdate(id, record);
  }

  private async transformStoragePathToUrl(
    doc: Document,
  ): Promise<IStorageDependentMany<T>> {
    const record: T = doc['_doc'];
    const storagePath = record.storagePaths;
    record.id = record['_id'];
    delete record['_id'];
    delete record['__v'];
    delete record['storagePaths'];

    return {
      ...record,
      files: await Promise.all(
        storagePath.map(async (path) => {
          return {
            url: await this.filesService
              .getDownloadLink(path)
              .catch(() => null),
            storagePath: path,
          };
        }),
      ),
    };
  }
}
