import { NotFoundException } from '@nestjs/common';
import { FilterQuery, Model } from 'mongoose';
import { SseService } from '../../controllers/sse/sse.service';
import {
  DEFAULT_GET_MANY_OPTIONS,
  DEFAULT_GET_ONE_OPTIONS,
  DEFAULT_UPDATE_OPTIONS,
} from '../constants/mongo-db.constant';
import {
  DBParams,
  DefaultResponse,
  Dictionary,
  GetManyOptions,
  GetOneOptions,
  ReceivedDocuments,
  UpdateOptions,
} from '../interfaces';
import { IFilterValue } from '../interfaces/filters.interface';
import { DateUtils, MongoDbUtil } from '../utils';
import { SchemaBase } from './schema.base';

export abstract class MongoDbBase<T extends SchemaBase> {
  private readonly collectionName = this.model.collection.name;
  protected constructor(
    protected model: Model<T>,
    private sseService: SseService,
  ) {}

  protected createNewRecord(data: T): Promise<T> {
    const newRecord = new this.model({
      ...data,
      createdAt: DateUtils.nowISODate(),
    });

    return newRecord.save().then((record) => {
      this.sendUpdateEvent();

      return record as T;
    });
  }

  protected getById(id: string, dbParams: DBParams = {}): Promise<T> {
    return MongoDbUtil.buildQuery<T>(this.model.findById(id), dbParams).then(
      (record) => {
        if (!record) {
          throw new NotFoundException();
        }

        return record;
      },
    );
  }

  protected getOne(options: GetOneOptions<T> = {}): Promise<T> {
    const { filter, dbParams } = { ...DEFAULT_GET_ONE_OPTIONS, ...options };

    return MongoDbUtil.buildQuery<T>(this.model.findOne(filter), dbParams);
  }

  protected findByIdAndUpdate(
    id: string,
    updatedModel: Partial<T>,
    options?: UpdateOptions,
  ): Promise<T> {
    const { dbParams, dontTouchEditedAt } = {
      ...DEFAULT_UPDATE_OPTIONS,
      ...options,
    };

    return MongoDbUtil.buildQuery(
      this.model.findByIdAndUpdate(
        id,
        {
          ...updatedModel,
          ...(dontTouchEditedAt ? {} : { editedAt: DateUtils.nowISODate() }),
        },
        { new: true },
      ),
      dbParams,
    ).then((result) => {
      this.sendUpdateEvent();

      return result as T;
    });
  }

  protected async getMany(
    options: GetManyOptions<T> = {},
  ): Promise<ReceivedDocuments<T>> {
    const { filter, dbParams, queryParams } = {
      ...DEFAULT_GET_MANY_OPTIONS,
      ...options,
    };

    let adapterFilterQueries: Dictionary<IFilterValue> = {};

    const { filters, ...paginateQueryParams } = queryParams;

    if (filters) {
      adapterFilterQueries = MongoDbUtil.adaptFilterQuery(
        JSON.parse(filters as string),
      );
    }

    const paginateParams = MongoDbUtil.getPaginateQueryParams(
      paginateQueryParams || DEFAULT_GET_MANY_OPTIONS.queryParams,
    );

    const finalFilter = { ...adapterFilterQueries, ...filter };

    const totalElements = await this.model.find(finalFilter).countDocuments();

    return MongoDbUtil.buildQuery<T[]>(this.model.find(finalFilter), {
      ...dbParams,
      ...paginateParams,
    })
      .exec()
      .then(async (elements) => {
        const pageSize = +queryParams?.pageSize || elements.length;

        return {
          elements,
          totalElements: totalElements,
          pageNumber: +queryParams?.pageNumber || 1,
          pageSize,
          totalPages: Math.ceil(totalElements / pageSize) || 0,
        };
      });
  }

  protected findByIdAndDelete(id: string): Promise<DefaultResponse> {
    return this.model.findByIdAndDelete(id).then(() => {
      this.sendUpdateEvent();

      return { success: true };
    });
  }

  checkAlreadyExist(filter: FilterQuery<T>): Promise<boolean> {
    return this.getOne({ filter }).then((result) => !result);
  }

  private sendUpdateEvent() {
    this.sseService.update({ collection: this.collectionName });
  }
}
