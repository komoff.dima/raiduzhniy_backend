import { FilterQuery, PopulateOptions } from 'mongoose';
import { OrderDirection } from '../enums';
import { Dictionary } from './dictionary.interface';
import { IFilterValue } from './filters.interface';

export interface PaginateQueryParams {
  orderBy?: string;
  orderDirection?: OrderDirection;
  pageNumber?: string;
  pageSize?: string;
}

export type StringifiedFilters = string;

export interface QueryParams extends PaginateQueryParams {
  filters?: Dictionary<IFilterValue> | StringifiedFilters;
}

export interface GetOneOptions<T> {
  filter?: FilterQuery<T>;
  dbParams?: DBParams;
}

export interface GetManyOptions<T> extends GetOneOptions<T> {
  queryParams?: QueryParams;
}

export interface DBParams {
  select?: string;
  populate?: PopulateOptions | (PopulateOptions | string)[];
}

export interface UpdateOptions {
  dontTouchEditedAt?: boolean;
  dbParams?: DBParams;
}

export interface PaginateParams {
  sort?: { [key: string]: string };
  skip?: number;
  limit?: number;
}

export type BuildQueryParams = DBParams & PaginateParams;
