export interface IRange<Min = string, Max = string> {
  min?: Min;
  max?: Max;
}
