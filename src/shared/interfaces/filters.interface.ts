import { FilterType } from '../enums';
import { IRange } from './range.interface';

export type FilterTypeMatcher<T> = {
  [key in FilterType]: T;
};

export interface FilterTypeDependentValue<Type, Value> {
  type: Type;
  value: Value;
}

export type IFilterValue =
  | FilterTypeDependentValue<FilterType.Search, string>
  | FilterTypeDependentValue<FilterType.Options, string[]>
  | FilterTypeDependentValue<FilterType.DateRange, IRange>;
