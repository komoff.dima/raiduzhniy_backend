import {
  StorageDependentManySchemaBase,
  StorageDependentSchemaBase,
} from '../abstract-classes';
import { ReceivedDocuments } from './documents.interface';
import { DBParams, GetManyOptions } from './mongo-db.interface';
import { DefaultResponse } from './response.interface';

export type IFile = Express.Multer.File;

export interface FileDto {
  url: string;
  storagePath: string;
}

export interface StorageDependent<Schema, Interface> {
  createRecord<Dto>(dto: Dto, ...args): Promise<Schema>;

  deleteRecord(id: string): Promise<DefaultResponse>;

  editRecord<EditDto>(id: string, editDto: EditDto, ...args): Promise<Schema>;

  getRecords(
    options?: GetManyOptions<Schema>,
  ): Promise<ReceivedDocuments<Interface>>;

  getRecord(id: string, dbParams?: DBParams): Promise<Interface>;
}

export type IStorageDependentOne<T extends StorageDependentSchemaBase> = Omit<
  T,
  'storagePath'
> & { file: FileDto };

export type IStorageDependentMany<T extends StorageDependentManySchemaBase> =
  Omit<T, 'storagePaths'> & { files: FileDto[] };

export interface SharpOptions {
  size?: {
    width: number;
    height?: number;
  };
  quality?: number;
}
