export interface HtmlDocumentAttributes {
  id: string;
  uniqueName: string;
}
