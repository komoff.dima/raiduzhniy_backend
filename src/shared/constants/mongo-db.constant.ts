import { FilterType } from '../enums';
import {
  GetManyOptions,
  GetOneOptions,
  IRange,
  UpdateOptions,
} from '../interfaces';
import { FilterTypeMatcher } from '../interfaces/filters.interface';

export const DEFAULT_GET_ONE_OPTIONS = {
  filter: {},
  dbParams: {},
} as GetOneOptions<unknown>;

export const DEFAULT_GET_MANY_OPTIONS = {
  filter: {},
  dbParams: {},
  queryParams: {
    pageSize: '50',
  },
} as GetManyOptions<unknown>;

export const DEFAULT_UPDATE_OPTIONS = {
  dontTouchEditedAt: false,
  dbParams: {},
} as UpdateOptions;

export const FILTER_QUERIES_MAPPER: Partial<
  FilterTypeMatcher<(value: unknown) => unknown>
> = {
  [FilterType.Search]: (value: string) => ({
    $regex: `${value}`,
    $options: 'i',
  }),
  [FilterType.DateRange]: ({ min, max }: IRange) => ({
    ...(min ? { $gte: min } : {}),
    ...(max ? { $lte: max } : {}),
  }),
};
