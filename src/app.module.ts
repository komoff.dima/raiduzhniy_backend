import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { APP_GUARD } from '@nestjs/core';
import { JwtModule } from '@nestjs/jwt';
import { MongooseModule } from '@nestjs/mongoose';
import { MulterModule } from '@nestjs/platform-express';
import { AuthGuard } from '@shared/guards/auth.guard';
import { memoryStorage } from 'multer';
import { LoggerModule } from 'nestjs-pino';
import { AboutUsModule } from './controllers/about-us';
import { AuthModule } from './controllers/auth';
import { ContactsModule } from './controllers/contacts';
import { DocumentTypesModule } from './controllers/document-types/document-types.module';
import { DocumentsModule } from './controllers/documents/documents.module';
import { NewsModule } from './controllers/news';
import { SseModule } from './controllers/sse/sse.module';
import { UsersModule } from './controllers/users';
import configuration from './environments/configuration.environment';

@Module({
  imports: [
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        secret: configService.get<string>('jwtSecret'),
        signOptions: {
          expiresIn: '7d',
        },
      }),
      inject: [ConfigService],
      global: true,
    }),
    ConfigModule.forRoot({
      isGlobal: true,
      load: [configuration],
    }),
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => {
        return {
          uri: configService.get<string>('mongoDbConnectionUrl'),
        };
      },
      inject: [ConfigService],
    }),
    LoggerModule.forRoot({
      pinoHttp: {
        transport: {
          target: 'pino-pretty',
        },
        level: 'debug',
        serializers: {
          req: (req) => ({
            id: req.id,
            method: req.method,
            url: req.url,
          }),
        },
      },
    }),
    MulterModule.register({
      storage: memoryStorage(),
    }),
  ],
  exports: [JwtModule],
})
class CoreModule {}

@Module({
  imports: [
    CoreModule,
    UsersModule,
    AuthModule,
    AboutUsModule,
    ContactsModule,
    NewsModule,
    SseModule,
    DocumentsModule,
    DocumentTypesModule,
  ],
  providers: [
    {
      provide: APP_GUARD,
      useClass: AuthGuard,
    },
  ],
})
export class AppModule {}
