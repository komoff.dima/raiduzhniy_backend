import { Environment } from './environment.interface';

export default (): Environment => ({
  port: 3333,
  apiUrl: '/api',
  jwtSecret: process.env.JWT_SECRET,
  telegramBotToken: process.env.TELEGRAM_BOT_TOKEN,
  telegramGroupId: +process.env.TELEGRAM_GROUP_ID,
  frontendBaseUrl: process.env.FRONTEND_BASE_URL,
  mongoDbConnectionUrl: process.env.MONGO_DB_CONNECTION_URL,
  contactsDocId: process.env.CONTACTS_DOC_ID,
  aboutUsDocId: process.env.ABOUT_US_DOC_ID,
});
