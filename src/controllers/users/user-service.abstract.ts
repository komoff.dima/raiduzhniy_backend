import {
  DefaultResponse,
  ReceivedDocuments,
  PaginateQueryParams,
} from '@shared/interfaces';
import { User } from './user.schema';
import { UserDto, UserUpdateDto } from './users.dto';

export interface IUserService {
  getUsers(queryParams: PaginateQueryParams): Promise<ReceivedDocuments<User>>;
  getUserById(
    userId: string,
    removeFields?: (keyof User)[],
    addFields?: (keyof User)[],
  ): Promise<User>;
  getUserByLogin(login: string): Promise<User>;
  createUser(userDto: UserDto): Promise<User>;
  updateUser(userId: string, userUpdateDto: UserUpdateDto): Promise<User>;
  updateLastActivity(userId: string): Promise<User>;
  confirmUserAgreement(userId: string): Promise<DefaultResponse>;
  changePassword(userId: string, newPassword: string): Promise<User>;
  resetPassword(userId: string): Promise<User>;
}
