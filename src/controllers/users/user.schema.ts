import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { SchemaBase } from '@shared/abstract-classes';
import { SchemaUtils } from '@shared/utils';
import mongoose, { HydratedDocument } from 'mongoose';
import { Address } from '../adresses/address.schema';
import { UserRole } from './users.enum';

export type UserSchema = HydratedDocument<User>;

@Schema()
export class User extends SchemaBase {
  @Prop({
    type: String,
    required: true,
    unique: true,
    dropDups: true,
    immutable: false,
  })
  login: string;

  @Prop({ type: String, required: true, select: false })
  passwordHash?: string;

  @Prop([{ type: String, required: true }])
  roles: UserRole[];

  @Prop({ type: Boolean, default: false })
  confirmedUserAgreement: boolean;

  @Prop({ type: String, default: null, select: false })
  lastActivity?: string | null;

  @Prop([{ type: mongoose.Schema.Types.ObjectId, ref: 'Address' }])
  addresses?: (Address | string)[];
}

export const UserSchema = SchemaFactory.createForClass(User);

SchemaUtils.setup(UserSchema);
