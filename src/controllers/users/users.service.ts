import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { MongoDbBase } from '@shared/abstract-classes/mongo-db.base';
import {
  DBParams,
  DefaultResponse,
  PaginateQueryParams,
  ReceivedDocuments,
} from '@shared/interfaces';
import { DateUtils } from '@shared/utils';
import * as bcrypt from 'bcrypt';
import { Model } from 'mongoose';
import { SseService } from '../sse/sse.service';
import { User } from './user.schema';
import { UserDto, UserUpdateDto } from './users.dto';

@Injectable()
export class UsersService extends MongoDbBase<User> {
  constructor(
    @InjectModel(User.name) model: Model<User>,
    sseService: SseService,
  ) {
    super(model, sseService);
  }

  async changePassword(userId: string, newPassword: string): Promise<User> {
    return this.findByIdAndUpdate(userId, {
      passwordHash: await this.createPasswordHash(newPassword),
    });
  }

  confirmUserAgreement(userId: string): Promise<DefaultResponse> {
    return this.findByIdAndUpdate(userId, {
      confirmedUserAgreement: true,
    })
      .then(() => ({ success: true }))
      .catch(() => ({ success: false }));
  }

  async createUser({
    roles,
    login,
    password,
    addresses,
  }: UserDto): Promise<User> {
    const existingUser = await this.getOne({ filter: { login } });

    if (existingUser) {
      throw new BadRequestException('Користувач з таким логіном вже існує');
    }

    const passwordHash = await this.createPasswordHash(password);

    return this.createNewRecord({
      login,
      roles,
      addresses,
      passwordHash,
    } as User);
  }

  getUserById(userId: string, dbParams?: DBParams): Promise<User> {
    return this.getById(userId, dbParams);
  }

  getUserByLogin(login: string): Promise<User> {
    return this.getOne({
      filter: { login },
      dbParams: { populate: ['addresses'], select: '+passwordHash' },
    });
  }

  getUsers(queryParams: PaginateQueryParams): Promise<ReceivedDocuments<User>> {
    return this.getMany({
      queryParams: queryParams,
      dbParams: { select: '+lastActivity', populate: ['addresses'] },
    });
  }

  async resetPassword(userId: string): Promise<User> {
    const user = await this.getUserById(userId);

    if (!user) {
      throw new NotFoundException();
    }

    return this.findByIdAndUpdate(userId, {
      passwordHash: await this.createPasswordHash(user.login),
    });
  }

  updateLastActivity(userId: string): Promise<User> {
    return this.findByIdAndUpdate(
      userId,
      {
        lastActivity: DateUtils.nowISODate(),
      },
      {
        dontTouchEditedAt: true,
        dbParams: { populate: ['addresses'] },
      },
    );
  }

  async updateUser(
    userId: string,
    userUpdateDto: UserUpdateDto,
  ): Promise<User> {
    const user = await this.model
      .findByIdAndUpdate(userId, userUpdateDto, {
        new: true,
      })
      .catch(() => null);

    if (!user) {
      throw new NotFoundException();
    }

    return user;
  }

  private async createPasswordHash(password: string): Promise<string> {
    const salt = await bcrypt.genSalt();

    return bcrypt.hashSync(password, salt);
  }
}
