import { IsAlreadyExist } from '@shared/validators';
import { IsNotEmpty, IsString } from 'class-validator';

export interface IDocumentType {
  name: string;
}

export class DocumentTypeDto implements IDocumentType {
  @IsAlreadyExist({ regexpOptions: 'i' })
  @IsNotEmpty()
  @IsString()
  name: string;
}

export class EditDocumentTypeDto implements IDocumentType {
  @IsNotEmpty()
  @IsString()
  name: string;
}
