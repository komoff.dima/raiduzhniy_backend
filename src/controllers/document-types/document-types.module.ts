import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { createIsAlreadyExistValidatorProviders } from '@shared/validators';
import { DocumentType, DocumentTypeSchema } from './document-type.schema';
import { DocumentTypesController } from './document-types.controller';
import { DocumentTypesService } from './document-types.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: DocumentType.name, schema: DocumentTypeSchema },
    ]),
  ],
  controllers: [DocumentTypesController],
  providers: [
    DocumentTypesService,
    ...createIsAlreadyExistValidatorProviders(DocumentTypesService),
  ],
  exports: [DocumentTypesService],
})
export class DocumentTypesModule {}
