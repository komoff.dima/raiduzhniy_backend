import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { SchemaBase } from '@shared/abstract-classes';
import { SchemaUtils } from '@shared/utils';
import { HydratedDocument } from 'mongoose';

export type DocumentTypeSchema = HydratedDocument<DocumentType>;

@Schema()
export class DocumentType extends SchemaBase {
  @Prop({ type: String, required: true, unique: true })
  name: string;
}

export const DocumentTypeSchema = SchemaFactory.createForClass(DocumentType);

SchemaUtils.setup(DocumentTypeSchema);
