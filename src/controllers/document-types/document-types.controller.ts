import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import { IsPublicRequest, Roles } from '@shared/decorators';
import { RolesGuard } from '@shared/guards';
import {
  DefaultResponse,
  PaginateQueryParams,
  ReceivedDocuments,
} from '@shared/interfaces';
import { Promise } from 'mongoose';
import { UserRole } from '../users/users.enum';
import { DocumentType } from './document-type.schema';
import { DocumentTypeDto, EditDocumentTypeDto } from './document-types.dto';
import { DocumentTypesService } from './document-types.service';

@Controller('document-types')
@UseGuards(RolesGuard)
export class DocumentTypesController {
  constructor(private documentTypesService: DocumentTypesService) {}

  @Get('')
  @IsPublicRequest()
  getDocumentTypes(
    @Query() queryParams?: PaginateQueryParams,
  ): Promise<ReceivedDocuments<DocumentType>> {
    return this.documentTypesService.getDocumentTypes(queryParams);
  }

  @Get(':id')
  @Roles(UserRole.Superadmin)
  getDocumentType(@Param('id') id: string): Promise<DocumentType> {
    return this.documentTypesService.getDocumentType(id);
  }

  @Post('create')
  @Roles(UserRole.Superadmin)
  createDocumentType(
    @Body() documentTypeDto: DocumentTypeDto,
  ): Promise<DocumentType> {
    return this.documentTypesService.createDocumentType(documentTypeDto);
  }

  @Post('validate-name')
  @Roles(UserRole.Superadmin)
  validateName(@Body() documentTypeDto: DocumentTypeDto): DefaultResponse {
    return { success: true };
  }

  @Delete(':id/delete')
  @Roles(UserRole.Superadmin)
  deleteDocumentType(@Param('id') id: string): Promise<DefaultResponse> {
    return this.documentTypesService.deleteDocumentType(id);
  }

  @Put(':id/edit')
  @Roles(UserRole.Superadmin)
  editDocumentType(
    @Param('id') id: string,
    @Body() documentTypeDto: EditDocumentTypeDto,
  ): Promise<DocumentType> {
    return this.documentTypesService.editDocumentType(id, documentTypeDto);
  }
}
