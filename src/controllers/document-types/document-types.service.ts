import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { MongoDbBase } from '@shared/abstract-classes/mongo-db.base';
import {
  DefaultResponse,
  PaginateQueryParams,
  ReceivedDocuments,
} from '@shared/interfaces';
import { Model } from 'mongoose';
import { SseService } from '../sse/sse.service';
import { DocumentType } from './document-type.schema';
import { DocumentTypeDto } from './document-types.dto';

@Injectable()
export class DocumentTypesService extends MongoDbBase<DocumentType> {
  constructor(
    @InjectModel(DocumentType.name) documentTypeModel: Model<DocumentType>,
    sseService: SseService,
  ) {
    super(documentTypeModel, sseService);
  }

  getDocumentType(id: string): Promise<DocumentType> {
    return this.getById(id);
  }

  getDocumentTypes(
    queryParams?: PaginateQueryParams,
  ): Promise<ReceivedDocuments<DocumentType>> {
    return this.getMany({
      queryParams: queryParams,
      dbParams: {
        select: '+createdAt +editedAt',
      },
    });
  }
  createDocumentType(documentTypeDto: DocumentTypeDto): Promise<DocumentType> {
    return this.createNewRecord(documentTypeDto);
  }

  deleteDocumentType(docTypeId: string): Promise<DefaultResponse> {
    return this.findByIdAndDelete(docTypeId);
  }

  editDocumentType(
    docTypeId: string,
    documentTypeDto: DocumentTypeDto,
  ): Promise<DocumentType> {
    return this.findByIdAndUpdate(docTypeId, documentTypeDto);
  }
}
