import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { FilesModule } from '@shared/modules/images/files.module';
import { DocumentTypesModule } from '../document-types/document-types.module';
import { Document, DocumentSchema } from './document.schema';
import { DocumentsController } from './documents.controller';
import { DocumentsService } from './documents.service';
import { IsExistingDocumentTypeConstraint } from './validators/existing-document-type.validator';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Document.name, schema: DocumentSchema },
    ]),
    FilesModule,
    DocumentTypesModule,
  ],
  providers: [DocumentsService, IsExistingDocumentTypeConstraint],
  controllers: [DocumentsController],
})
export class DocumentsModule {}
