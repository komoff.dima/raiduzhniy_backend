import {
  Body,
  Controller,
  Delete,
  FileTypeValidator,
  Get,
  Param,
  ParseFilePipe,
  Post,
  Put,
  Query,
  UploadedFile,
  UploadedFiles,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor, FilesInterceptor } from '@nestjs/platform-express';
import { IsPublicRequest, Roles } from '@shared/decorators';
import { RolesGuard } from '@shared/guards';
import {
  DefaultResponse,
  IFile,
  QueryParams,
  ReceivedDocuments,
} from '@shared/interfaces';
import { SharpFilesPipe } from '@shared/pipes';
import { UserRole } from '../users/users.enum';
import {
  DeleteDocumentFileDto,
  DocumentDto,
  EditDocumentDto,
} from './document.dto';
import { Document, IDocument } from './document.schema';
import { DocumentsService } from './documents.service';

@Controller('documents')
@UseGuards(RolesGuard)
export class DocumentsController {
  constructor(private documentsService: DocumentsService) {}

  @Post('create')
  @Roles(UserRole.Admin, UserRole.Superadmin)
  @UseInterceptors(FilesInterceptor('files'))
  createDocument(
    @Body() documentDto: DocumentDto,
    @UploadedFiles(
      new ParseFilePipe({
        validators: [
          new FileTypeValidator({
            fileType: '.(jpg|jpeg|png|doc|docx|pdf)',
          }),
        ],
      }),
      SharpFilesPipe,
    )
    files: IFile[],
  ): Promise<Document> {
    return this.documentsService.createRecord(documentDto, files);
  }

  @Put(':id/add-file')
  @Roles(UserRole.Admin, UserRole.Superadmin)
  @UseInterceptors(FileInterceptor('file'))
  addFile(
    @Param('id') id: string,
    @UploadedFile(
      new ParseFilePipe({
        validators: [
          new FileTypeValidator({
            fileType: '.(jpg|jpeg|png|doc|docx|pdf)',
          }),
        ],
      }),
    )
    file: IFile,
  ): Promise<Document> {
    return this.documentsService.addFileToRecord(id, file);
  }

  @Put(':id/edit')
  @Roles(UserRole.Admin, UserRole.Superadmin)
  @UseInterceptors(FilesInterceptor('files'))
  editDocument(
    @Param('id') id: string,
    @Body() documentDto: EditDocumentDto,
    @UploadedFiles(
      new ParseFilePipe({
        validators: [
          new FileTypeValidator({
            fileType: '.(jpg|jpeg|png|doc|docx|pdf)',
          }),
        ],
        fileIsRequired: false,
      }),
    )
    files: IFile[],
  ): Promise<Document> {
    return this.documentsService.editRecord(id, documentDto, files);
  }

  @Put(':id/delete-file')
  @Roles(UserRole.Admin, UserRole.Superadmin)
  deleteFile(
    @Param('id') id: string,
    @Body() deleteFileDto: DeleteDocumentFileDto,
  ): Promise<Document> {
    return this.documentsService.deleteFileFromRecord(
      id,
      deleteFileDto.storagePath,
    );
  }

  @Get('')
  @IsPublicRequest()
  getDocuments(
    @Query() queryParams: QueryParams,
  ): Promise<ReceivedDocuments<IDocument>> {
    return this.documentsService.getDocuments(queryParams);
  }

  @Get(':id')
  getDocument(@Param('id') id: string): Promise<IDocument> {
    return this.documentsService.getDocument(id);
  }

  @Delete(':id/delete')
  @Roles(UserRole.Admin, UserRole.Superadmin)
  deleteDocument(@Param('id') id: string): Promise<DefaultResponse> {
    return this.documentsService.deleteRecord(id);
  }
}
