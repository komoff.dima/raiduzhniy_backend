import { Injectable } from '@nestjs/common';
import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { isValidObjectId } from 'mongoose';
import { DocumentTypesService } from '../../document-types/document-types.service';

@ValidatorConstraint({ name: 'isExistingDocumentType', async: true })
@Injectable()
export class IsExistingDocumentTypeConstraint
  implements ValidatorConstraintInterface
{
  constructor(private documentTypesService: DocumentTypesService) {}

  validate(documentTypeId: string): Promise<boolean> | boolean {
    if (!isValidObjectId(documentTypeId)) {
      return false;
    }

    return this.documentTypesService
      .getDocumentType(documentTypeId)
      .then((docType) => !!docType);
  }

  defaultMessage(validationArguments?: ValidationArguments): string {
    return `${validationArguments.property} incorrect document type id`;
  }
}

export function IsExistingDocumentType(validationOptions?: ValidationOptions) {
  return function (object: unknown, propertyName: string) {
    registerDecorator({
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      constraints: [],
      validator: IsExistingDocumentTypeConstraint,
    });
  };
}
