import { IsArrayOrString } from '@shared/validators';
import {
  IsDateString,
  IsNotEmpty,
  IsOptional,
  IsString,
} from 'class-validator';
import { IsExistingDocumentType } from './validators/existing-document-type.validator';

export class DocumentDto {
  @IsNotEmpty()
  @IsString()
  name: string;

  @IsOptional()
  @IsString()
  description?: string;

  @IsNotEmpty()
  @IsString()
  @IsExistingDocumentType()
  type: string;

  @IsNotEmpty()
  @IsString()
  @IsDateString()
  approvedAt: string;
}

export class EditDocumentDto extends DocumentDto {
  @IsOptional()
  @IsArrayOrString()
  filesToDelete: string[] | string;
}

export class DeleteDocumentFileDto {
  @IsNotEmpty()
  @IsString()
  storagePath: string;
}
