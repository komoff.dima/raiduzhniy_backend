import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { StorageDependentManySchemaBase } from '@shared/abstract-classes';
import { IStorageDependentMany } from '@shared/interfaces';
import { SchemaUtils } from '@shared/utils';
import mongoose, { HydratedDocument } from 'mongoose';

export type DocumentTypeSchema = HydratedDocument<Document>;

@Schema()
export class Document extends StorageDependentManySchemaBase {
  @Prop({ type: String, required: true })
  name: string;

  @Prop({ type: String })
  description: string;

  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: 'DocumentType',
    required: true,
  })
  type: string;

  @Prop({ type: String, required: true })
  approvedAt: string;
}

export type IDocument = IStorageDependentMany<Document>;

export const DocumentSchema = SchemaFactory.createForClass(Document);

SchemaUtils.setup(DocumentSchema);
