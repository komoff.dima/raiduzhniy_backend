import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { StorageDependentMany } from '@shared/abstract-classes';
import { QueryParams, ReceivedDocuments } from '@shared/interfaces';
import { StorageType } from '@shared/modules/firebase/storage';
import { FilesService } from '@shared/modules/images/files.service';
import { Model } from 'mongoose';
import { SseService } from '../sse/sse.service';
import { Document, IDocument } from './document.schema';

@Injectable()
export class DocumentsService extends StorageDependentMany<Document> {
  protected readonly storageType = StorageType.Documents;
  constructor(
    @InjectModel(Document.name) documentTypeModel: Model<Document>,
    sseService: SseService,
    filesService: FilesService,
  ) {
    super(documentTypeModel, sseService, filesService);
  }

  async getDocuments(
    queryParams?: QueryParams,
  ): Promise<ReceivedDocuments<IDocument>> {
    return this.getRecords({
      queryParams: queryParams,
      dbParams: {
        select: '+createdAt +editedAt',
        populate: ['type'],
      },
    });
  }

  async getDocument(id: string): Promise<IDocument> {
    return this.getRecord(id, {
      populate: ['type'],
    });
  }
}
