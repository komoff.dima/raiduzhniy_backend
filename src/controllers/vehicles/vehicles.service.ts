import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { MongoDbBase } from '@shared/abstract-classes/mongo-db.base';
import { DefaultResponse } from '@shared/interfaces';
import { Model } from 'mongoose';
import { SseService } from '../sse/sse.service';
import { Vehicle } from './vehicle.schema';
import { VehicleDto } from './vehicles.dto';

@Injectable()
export class VehiclesService extends MongoDbBase<Vehicle> {
  constructor(
    @InjectModel(Vehicle.name) vehicleModel: Model<Vehicle>,
    sseService: SseService,
  ) {
    super(vehicleModel, sseService);
  }

  createVehicle(
    vehicleDto: VehicleDto,
    addressIds?: string[],
  ): Promise<Vehicle> {
    return this.createNewRecord({
      ...vehicleDto,
      ...(addressIds ? { addresses: addressIds } : {}),
    } as Vehicle);
  }

  deleteVehicle(id: string): Promise<DefaultResponse> {
    return this.findByIdAndDelete(id);
  }
}
