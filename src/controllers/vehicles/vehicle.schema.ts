import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { SchemaBase } from '@shared/abstract-classes';
import { SchemaUtils } from '@shared/utils';
import mongoose, { HydratedDocument } from 'mongoose';
import { Address } from '../adresses/address.schema';
import { VehicleType } from './vehicles.enum';

export type VehicleSchema = HydratedDocument<Vehicle>;

@Schema()
export class Vehicle extends SchemaBase {
  @Prop({ type: String, required: true })
  type: VehicleType;

  @Prop({ type: String, required: true })
  brand: string;

  @Prop({ type: String, required: true })
  plateNumber: string;

  @Prop([
    { type: mongoose.Schema.Types.ObjectId, ref: 'Address', select: false },
  ])
  addresses: (Address | string)[];

  @Prop({ type: String })
  model?: string;
}

export const VehicleSchema = SchemaFactory.createForClass(Vehicle);

SchemaUtils.setup(VehicleSchema);
