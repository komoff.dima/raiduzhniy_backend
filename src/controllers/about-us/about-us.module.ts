import { Module } from '@nestjs/common';
import { HtmlUtils } from '@shared/utils';
import { HtmlsModule } from '@shared/modules/htmls';
import { AboutUsController } from './about-us.controller';

@Module({
  imports: [HtmlsModule],
  controllers: [AboutUsController],
  providers: HtmlUtils.getHtmlDocAttributesProviders('aboutUs'),
})
export class AboutUsModule {}
