import { Controller, Inject, UseGuards } from '@nestjs/common';
import { HtmlsControllerBase } from '@shared/abstract-classes';
import { RolesGuard } from '@shared/guards';
import { HtmlDocumentAttributes } from '@shared/interfaces';
import { HtmlsService } from '@shared/modules/htmls';
import { HTML_DOCUMENT_ATTRIBUTES } from '@shared/tokens';

@Controller('about-us')
@UseGuards(RolesGuard)
export class AboutUsController extends HtmlsControllerBase {
  constructor(
    htmlsService: HtmlsService,
    @Inject(HTML_DOCUMENT_ATTRIBUTES) attributes: HtmlDocumentAttributes,
  ) {
    super(htmlsService, attributes);
  }
}
