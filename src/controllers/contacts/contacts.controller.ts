import {
  Body,
  Controller,
  Delete,
  Get,
  Inject,
  Param,
  Post,
  UseGuards,
} from '@nestjs/common';
import { HtmlsControllerBase } from '@shared/abstract-classes';
import { IsPublicRequest, Roles } from '@shared/decorators';
import { RolesGuard } from '@shared/guards';
import {
  DefaultResponse,
  HtmlDocumentAttributes,
  ReceivedDocuments,
} from '@shared/interfaces';
import { HtmlsService } from '@shared/modules/htmls';
import { MapMarkerDto, MapMarker, MapService } from '@shared/modules/map';
import { HTML_DOCUMENT_ATTRIBUTES } from '@shared/tokens';
import { UserRole } from '../users/users.enum';

@Controller('contacts')
@UseGuards(RolesGuard)
export class ContactsController extends HtmlsControllerBase {
  constructor(
    htmlsService: HtmlsService,
    private mapService: MapService,
    @Inject(HTML_DOCUMENT_ATTRIBUTES) attributes: HtmlDocumentAttributes,
  ) {
    super(htmlsService, attributes);
  }

  @Get('markers')
  @IsPublicRequest()
  getMarkers(): Promise<ReceivedDocuments<MapMarker>> {
    return this.mapService.getMarkers();
  }

  @Post('markers')
  @Roles(UserRole.Admin, UserRole.Superadmin)
  createMarker(@Body() markerDto: MapMarkerDto): Promise<MapMarker> {
    return this.mapService.addMarker(markerDto);
  }

  @Delete('markers/:id/delete')
  @Roles(UserRole.Admin, UserRole.Superadmin)
  deleteMarker(@Param('id') id: string): Promise<DefaultResponse> {
    return this.mapService.deleteMarker(id);
  }
}
