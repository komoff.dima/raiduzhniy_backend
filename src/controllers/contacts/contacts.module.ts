import { Module } from '@nestjs/common';
import { HtmlsModule } from '@shared/modules/htmls';
import { MapModule } from '@shared/modules/map';
import { HtmlUtils } from '@shared/utils';
import { ContactsController } from './contacts.controller';

@Module({
  imports: [HtmlsModule, MapModule],
  controllers: [ContactsController],
  providers: HtmlUtils.getHtmlDocAttributesProviders('contacts'),
})
export class ContactsModule {}
