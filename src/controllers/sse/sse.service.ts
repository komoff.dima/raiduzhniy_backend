import { Injectable } from '@nestjs/common';
import { Observable, Subject } from 'rxjs';
import { UpdateEvent } from './sse.interface';

@Injectable()
export class SseService {
  private _updates$ = new Subject<string>();
  getUpdates(): Observable<string> {
    return this._updates$.asObservable();
  }

  update(event: UpdateEvent): void {
    this._updates$.next(JSON.stringify(event));
  }
}
