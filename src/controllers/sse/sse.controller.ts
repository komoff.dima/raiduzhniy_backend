import { Controller, Sse } from '@nestjs/common';
import { IsPublicRequest } from '@shared/decorators';
import { Observable } from 'rxjs';
import { SseService } from './sse.service';

@Controller('sse')
export class SseController {
  constructor(private sseService: SseService) {}
  @Sse('updates')
  @IsPublicRequest()
  getUpdates(): Observable<string> {
    return this.sseService.getUpdates();
  }
}
