import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { SchemaBase } from '@shared/abstract-classes';
import { SchemaUtils } from '@shared/utils';
import mongoose, { HydratedDocument } from 'mongoose';
import { Address } from '../adresses/address.schema';

export type OwnerSchema = HydratedDocument<Owner>;

@Schema()
export class Owner extends SchemaBase {
  @Prop({ type: String, required: true })
  name: string;

  @Prop({ type: String, default: '' })
  surname?: string;

  @Prop([{ type: String }])
  phoneNumbers?: string[];

  @Prop([
    { type: mongoose.Schema.Types.ObjectId, ref: 'Address', select: false },
  ])
  addresses: (Address | string)[];
}

export const OwnerSchema = SchemaFactory.createForClass(Owner);

SchemaUtils.setup(OwnerSchema);
