import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { MongoDbBase } from '@shared/abstract-classes/mongo-db.base';
import { DefaultResponse } from '@shared/interfaces';
import { Model } from 'mongoose';
import { SseService } from '../sse/sse.service';
import { Owner } from './owner.schema';
import { OwnerDto } from './owners.dto';

@Injectable()
export class OwnersService extends MongoDbBase<Owner> {
  constructor(
    @InjectModel(Owner.name) ownerModel: Model<Owner>,
    sseService: SseService,
  ) {
    super(ownerModel, sseService);
  }

  createOwner(ownerDto: OwnerDto, addressIds?: string[]): Promise<Owner> {
    return this.createNewRecord({
      ...ownerDto,
      ...(addressIds ? { addresses: addressIds } : {}),
    } as Owner);
  }

  deleteOwner(id: string): Promise<DefaultResponse> {
    return this.findByIdAndDelete(id);
  }
}
