import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { Roles } from '@shared/decorators';
import { RolesGuard } from '@shared/guards';
import { UserRole } from '../users/users.enum';
import { AddressDto } from './addresses.dto';
import { AddressesService } from './addresses.service';

@Controller('addresses')
@UseGuards(RolesGuard)
export class AddressesController {
  constructor(private addressesService: AddressesService) {}
  @Post('create')
  @Roles(UserRole.Superadmin)
  create(@Body() addressDto: AddressDto) {
    return this.addressesService.create(addressDto);
  }
}
