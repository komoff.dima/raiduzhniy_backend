import { Type } from 'class-transformer';
import {
  IsArray,
  IsEnum,
  IsNotEmpty,
  IsObject,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';
import { OwnerDto } from '../owners';
import { VehicleDto } from '../vehicles';
import { StreetHouseNumber } from './addresses.enum';

export class AddressDto {
  @IsNotEmpty()
  @IsEnum(StreetHouseNumber)
  streetHouseNumber: StreetHouseNumber;

  @IsNotEmpty()
  @IsString()
  apartment: string;

  @IsOptional()
  @IsArray()
  @IsObject({ each: true })
  @ValidateNested({ each: true })
  @Type(() => OwnerDto)
  owners?: OwnerDto[];

  @IsOptional()
  @IsArray()
  @IsObject({ each: true })
  @ValidateNested({ each: true })
  @Type(() => VehicleDto)
  vehicles?: VehicleDto[];
}
