import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { SchemaBase } from '@shared/abstract-classes';
import { SchemaUtils } from '@shared/utils';
import mongoose, { HydratedDocument } from 'mongoose';
import { Owner } from '../owners';
import { Vehicle } from '../vehicles';
import { StreetHouseNumber } from './addresses.enum';

export type UserSchema = HydratedDocument<Address>;

@Schema()
export class Address extends SchemaBase {
  @Prop({ type: String, required: true })
  streetHouseNumber: StreetHouseNumber;

  @Prop({ type: String, required: true })
  apartment: string;

  @Prop([{ type: mongoose.Schema.Types.ObjectId, ref: 'Owner' }])
  owners?: (Owner | string)[];

  @Prop([{ type: mongoose.Schema.Types.ObjectId, ref: 'Vehicle' }])
  vehicles?: (Vehicle | string)[];
}

export const AddressSchema = SchemaFactory.createForClass(Address);

SchemaUtils.setup(AddressSchema);
