import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { MongoDbBase } from '@shared/abstract-classes/mongo-db.base';
import { Model } from 'mongoose';
import { SseService } from '../sse/sse.service';
import { Address } from './address.schema';
import { AddressDto } from './addresses.dto';

@Injectable()
export class AddressesService extends MongoDbBase<Address> {
  constructor(
    @InjectModel(Address.name) addressModel: Model<Address>,
    sseService: SseService,
  ) {
    super(addressModel, sseService);
  }

  async create({ streetHouseNumber, apartment }: AddressDto): Promise<Address> {
    const existingAddress = await this.getOne({
      filter: {
        $and: [{ streetHouseNumber }, { apartment }],
      },
    });

    if (existingAddress) {
      return existingAddress;
    }

    return this.createNewRecord({ streetHouseNumber, apartment });
  }
}
