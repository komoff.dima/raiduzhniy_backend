import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { StorageDependentSchemaBase } from '@shared/abstract-classes';
import { IStorageDependentOne } from '@shared/interfaces';
import { SchemaUtils } from '@shared/utils';
import { HydratedDocument } from 'mongoose';

export type NewsSchema = HydratedDocument<News>;

@Schema()
export class News extends StorageDependentSchemaBase {
  @Prop({ type: String, default: '' })
  storagePath: string;

  @Prop({ type: String, required: true })
  title: string;

  @Prop({ type: String, required: true })
  html: string;
}

export type INews = IStorageDependentOne<News>;

export const NewsSchema = SchemaFactory.createForClass(News);

SchemaUtils.setup(NewsSchema);
