import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { SharpOptions } from '@shared/interfaces';
import { FilesModule } from '@shared/modules/images/files.module';
import { TelegramBotModule } from '@shared/modules/telegram-bot';
import { SHARP_OPTIONS } from '@shared/tokens/files.token';
import { NewsController } from './news.controller';
import { News, NewsSchema } from './news.schema';
import { NewsService } from './news.service';

@Module({
  imports: [
    FilesModule,
    TelegramBotModule,
    MongooseModule.forFeature([{ name: News.name, schema: NewsSchema }]),
  ],
  controllers: [NewsController],
  providers: [
    NewsService,
    {
      provide: SHARP_OPTIONS,
      useValue: { size: { width: 500 } } as SharpOptions,
    },
  ],
})
export class NewsModule {}
