import {
  Body,
  Controller,
  Delete,
  FileTypeValidator,
  Get,
  Param,
  ParseFilePipe,
  Post,
  Put,
  Query,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { IsPublicRequest, Roles } from '@shared/decorators';
import { RolesGuard } from '@shared/guards';
import {
  DefaultResponse,
  IFile,
  PaginateQueryParams,
  ReceivedDocuments,
} from '@shared/interfaces';
import { HtmlDto } from '@shared/modules/htmls';
import { SharpFilePipe } from '@shared/pipes';
import { UserRole } from '../users/users.enum';
import { EditNewsDto } from './news.dto';
import { INews, News } from './news.schema';
import { NewsService } from './news.service';

@Controller('news')
@UseGuards(RolesGuard)
export class NewsController {
  constructor(private newsService: NewsService) {}

  @Post('create')
  @UseInterceptors(FileInterceptor('file'))
  @Roles(UserRole.Admin, UserRole.Superadmin)
  async createNews(
    @Body() newsDto: HtmlDto,
    @UploadedFile(
      new ParseFilePipe({
        validators: [new FileTypeValidator({ fileType: '.(jpg|jpeg|png)' })],
        fileIsRequired: false,
      }),
      SharpFilePipe,
    )
    image?: IFile,
  ): Promise<News> {
    return this.newsService.createNews(newsDto, image);
  }

  @Put(':id/edit')
  @UseInterceptors(FileInterceptor('file'))
  @Roles(UserRole.Admin, UserRole.Superadmin)
  async editNews(
    @Body() newsDto: EditNewsDto,
    @Param('id') id: string,
    @UploadedFile(
      new ParseFilePipe({
        validators: [new FileTypeValidator({ fileType: /(jpg|jpeg|png)$/ })],
        fileIsRequired: false,
      }),
      SharpFilePipe,
    )
    image?: IFile,
  ): Promise<News> {
    return this.newsService.editNews(id, newsDto, image);
  }

  @Delete(':id/delete')
  @Roles(UserRole.Admin, UserRole.Superadmin)
  async deleteNews(@Param('id') id: string): Promise<DefaultResponse> {
    return this.newsService.deleteNews(id);
  }

  @Get()
  @IsPublicRequest()
  async getNews(
    @Query() queryParams?: PaginateQueryParams,
  ): Promise<ReceivedDocuments<INews>> {
    return this.newsService.getNews(queryParams);
  }

  @Get(':id')
  @IsPublicRequest()
  async getOneNews(@Param('id') id: string) {
    return this.newsService.getOneNews(id);
  }
}
