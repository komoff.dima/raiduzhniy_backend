import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { StorageDependentOne } from '@shared/abstract-classes';
import {
  DefaultResponse,
  IFile,
  PaginateQueryParams,
  ReceivedDocuments,
} from '@shared/interfaces';
import { StorageType } from '@shared/modules/firebase/storage';
import { HtmlDto } from '@shared/modules/htmls';
import { FilesService } from '@shared/modules/images/files.service';
import { TelegramBotService } from '@shared/modules/telegram-bot/telegram-bot.service';
import { Model } from 'mongoose';
import { SseService } from '../sse/sse.service';
import { EditNewsDto } from './news.dto';
import { INews, News } from './news.schema';

@Injectable()
export class NewsService extends StorageDependentOne<News> {
  protected readonly storageType = StorageType.Images;
  constructor(
    imagesService: FilesService,
    private telegramBotService: TelegramBotService,
    @InjectModel(News.name) newsModel: Model<News>,
    sseService: SseService,
  ) {
    super(newsModel, sseService, imagesService);
  }

  async createNews(newsDto: HtmlDto, image?: IFile): Promise<News> {
    return this.createRecord(newsDto, image).then((news) => {
      this.telegramBotService.createdNews(news.id, news.title);

      return news;
    });
  }

  async editNews(
    id: string,
    editNewsDto: EditNewsDto,
    newImage?: IFile,
  ): Promise<News> {
    return this.editRecord(id, editNewsDto, newImage);
  }

  async deleteNews(newsId: string): Promise<DefaultResponse> {
    return this.deleteRecord(newsId);
  }

  async getNews(
    queryParams?: PaginateQueryParams,
  ): Promise<ReceivedDocuments<INews>> {
    return this.getRecords({
      queryParams: queryParams,
      dbParams: {
        select: '+createdAt +editedAt',
      },
    });
  }

  async getOneNews(id: string): Promise<INews> {
    return this.getRecord(id);
  }
}
