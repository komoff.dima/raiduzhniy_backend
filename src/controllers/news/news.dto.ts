import { HtmlDto } from '@shared/modules/htmls';
import { IsNullString } from '@shared/validators';
import { IsOptional } from 'class-validator';

export class EditNewsDto extends HtmlDto {
  @IsOptional()
  @IsNullString()
  file?: null;
}
